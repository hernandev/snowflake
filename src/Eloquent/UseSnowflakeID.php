<?php

namespace Snowflake\Eloquent;

/**
 * Trait UseSnowflakeID.
 *
 * Provides Snowflake functionality for Models.
 *
 * @method getKeyName
 * @method getAttribute($keyName)
 * @method setAttribute($keyName, $value)
 */
trait UseSnowflakeID
{
    /**
     * Sets the record _id field with a new Snowflake ID.
     *
     * @return bool
     */
    public function setSnowflakeID()
    {
        // detect key name.
        $keyName = $this->getKeyName();
        // get current value, if any.
        $current = $this->getAttribute($keyName);

        // return false case already exists.
        if ($current) {
            return true;
        }

        // snowflake ID.
        $snowflakeRandomID = snowflake()->nextID();

        // set the attribute value being the snowflake id just created.
        return $this->setAttribute($keyName, $snowflakeRandomID);
    }
}