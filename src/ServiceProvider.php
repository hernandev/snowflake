<?php

namespace Snowflake;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 *
 * Snowflake IoC service provider.
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register resources.
     */
    public function register()
    {
        // bind snowflake instance by key.
        $this->app->singleton('snowflake', function () {
            return new Snowflake();
        });

        // bind snowflake instance by class name.
        $this->app->bind(Snowflake::class, 'snowflake');

        // require helpers file.
        require_once __DIR__ . '/helpers.php';
    }

    /**
     * Provides keys.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'snowflake',
            Snowflake::class,
        ];
    }
}