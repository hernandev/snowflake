<?php

namespace Snowflake\Parsers;

/**
 * Class MachineID
 *
 * Snowflake MachineID parser.
 */
class MachineID
{
    /**
     * Parse a MachineID or hostname into a UInt8Array with two elements.
     *
     * @param string|null $sourceMachineID
     *
     * @return int
     */
    public static function parse(string $sourceMachineID = null)
    {
        // try using machine ID provided or current server hostname.
        $machineID = $sourceMachineID ? $sourceMachineID : static::getHexHostname();

        // send back a string with two byte, in hex.
        return static::extractBytes($machineID);
    }

    /**
     * Complete with zeros to ensure there are two digits.
     *
     * @param string $value
     *
     * @return int
     */
    protected static function extractBytes(string $value = '')
    {
        // make sure there are four.
        $atLeast4HexDigits = str_pad(static::hexOnly($value), 4, '0', STR_PAD_RIGHT);

        // get two hex bytes.
        // get two hex bytes.
        $byte1 = hexdec(mb_substr($atLeast4HexDigits, 0, 2));
        $byte2 = hexdec(mb_substr($atLeast4HexDigits, 2, 2));

        return (($byte1 << 8) + ($byte2));
    }

    /**
     * Make sure a given string has only hexadecimal digits.
     *
     * @param string|null $value
     *
     * @return string
     */
    protected static function hexOnly(string $value = null)
    {
        return (string) preg_replace('/[^0-9a-f]+/i', '', ($value ?? '')) ?? '';
    }

    /**
     * Returns the current machine hostname, if any.
     * @return string
     */
    protected static function getHexHostname() : ?string
    {
        // get some local values.
        $hostname = gethostname();

        // increment sapi name into hostname if too short.
        if (mb_strlen($hostname) < 4) {
            $hostname = $hostname . php_sapi_name();
        }

        // return a normalized, at least 2 byte version of the hostname.
        return $hostname;
    }
}