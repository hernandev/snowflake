<?php

namespace Snowflake\Parsers;

use Carbon\Carbon;

/**
 * Class Epoch.
 *
 * String to epoch converter.
 */
class Epoch
{
    /**
     * @var string Sony's snowflake default.
     */
    protected static $defaultEpoch = '2014-09-01';

    /**
     * Parse a given epoch string, if any.
     *
     * @param string|null $epoch
     *
     * @return Carbon
     */
    public static function parse(string $epoch = null)
    {
        try {
            // parse input and get timestamp as integer (seconds).
            return Carbon::parse($epoch);
        } catch (\Exception $e) {
            // parse default case exception.
            return Carbon::parse(static::$defaultEpoch, 'UTC')->startOfDay();
        }
    }
}