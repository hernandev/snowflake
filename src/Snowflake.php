<?php

namespace Snowflake;

use Snowflake\Parsers\Epoch;
use Snowflake\Parsers\MachineID;

class Snowflake
{
    /**
     * @var int ID length for Snowflake.
     */
    protected $idLength = 63;

    /**
     * @var int Machine part bits length.
     */
    protected $machineLength = 16;

    /**
     * @var int Sequential value length (in bits, default 8).
     */
    protected $timeLength = 39;

    /**
     * @var int Time value length (in bits, default 39).
     */
    protected $sequenceLength = 8;

    /**
     * @var int
     */
    protected $epoch;

    /**
     * Fallback machine ID.
     *
     * @var int
     */
    protected $machineID;


    /**
     * Snowflake constructor.
     *
     * @param string|null $machineID Custom machine ID, if needed.
     * @param string $epoch Any Carbon compatible date string.
     */
    public function __construct(string $machineID = null, string $epoch = '2014-09-01')
    {
        // parse and assign machine ID.
        $this->machineID = MachineID::parse($machineID);
        // parse and assign Epoch timestamp.
        $this->epoch = $this->snowflakeTimestamp(Epoch::parse($epoch)->timestamp);
    }

    /**
     * Generate the Snowflake ID.
     *
     * @return int
     */
    public function nextID()
    {
        // machineID is taken as it is.
        $machineIDPart = $this->getMachineIDPart();
        // sequence part is applied to machine length.
        $sequencePart  = $this->getSequencePart() <<  $this->machineLength;
        // time part is applied to both machine length and sequence length (sum both).
        $timePart      = $this->getTimePart()     <<  ($this->machineLength + $this->sequenceLength);

        // now compose the ID.
        return $timePart | $sequencePart | $machineIDPart;
    }

    /**
     * Convert a given value (current, epoch) into Snowflake timestamp (12 digits).
     *
     * @param int|float $value
     *
     * @return int
     */
    protected function snowflakeTimestamp($value)
    {
        return (int) ($value * 100);
    }

    /**
     * @return int Current timestamp to derive the ID.
     */
    protected function currentTimestamp()
    {
        return $this->snowflakeTimestamp(microtime(true));
    }

    /**
     * Calculate the difference between epoch and now.
     *
     * @return int
     */
    protected function getMachineIDPart()
    {
        // calculate the difference between epoch and now.
        return $this->machineID;
    }

    /**
     * Use random sequence since php does not have
     * state between requests.
     *
     * @return int
     */
    protected function getSequencePart()
    {
        // create the mask to apply on the sequence value.
        $sequenceMask = (1 << $this->sequenceLength) - 1;

         try {
             // generate a random integer to use as sequence.
             $sequenceRandomValue = hexdec(bin2hex(random_bytes(4)));
         } catch (\Exception $e) {
             // capture exception message.
             capture_exception($e);
             // use a simple random value.
             $sequenceRandomValue = mt_rand(1024, 4096);
         };

        // calculate and return the masked sequence value.
        return $sequenceRandomValue & $sequenceMask;
    }

    /**
     * Calculate the difference between epoch and now.
     *
     * @return int
     */
    protected function getTimePart()
    {
        // calculate the difference between epoch and now.
        return $this->currentTimestamp() - $this->epoch;
    }
}
