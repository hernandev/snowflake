<?php

/**
 * Define Snowflake helper function.
 */
if (!function_exists('snowflake') && function_exists('app')) {
    /**
     * @return \Snowflake\Snowflake
     */
    function snowflake()
    {
        // get instance from IoC.
        return app()->make('snowflake');
    }
}