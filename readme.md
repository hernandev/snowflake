# Sony Snowflake inspired PHP ID Generator.


Model usage example:

```php
<?php

use Snowflake\Eloquent\UseSnowflakeID;

class User extends Model
{
    // enable snowflake methods.
    use UseSnowflakeID;

    // set as auto id.
    protected $snowflake = true;

    /**
     * Model boot.
     */
    protected static function boot()
    {
        // register creating observers.
        static::creating(function (Model $model) {
            // when snowflake is enabled...
            if ($this->snowflake == true) {
                // set snowflake id.
                $model->setSnowflakeID();
            }
        });

        // call parent logic.
        parent::boot();
    }
}

```